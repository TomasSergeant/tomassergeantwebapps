var expect   = require('chai').expect;
var app      = require('../app.js');
var request  = require('supertest');
var agent    = request.agent(app);
var User = require('../models/Posts');

describe('GET  all /posts', function() {
  it('should respond with 200 in case of valid requets', function(done) {
    agent.get('/posts')
      .send()
      .end(function(err, res) {
        if (err) { return done(err); }
        //console.log('res: ', res);
        var fetchedData = JSON.parse(res.text);

        expect(fetchedData).to.be.an('array');
        expect(fetchedData).to.not.empty;

        var post = fetchedData[0];

        if(post) {
          expect(post).to.have.any.keys('__v', '_id', 'comments', 'upvotes');
          expect(post.comments).to.be.an('array');
          expect(post.upvotes).to.be.a('number');
          done();
        }
      });
  });
});


describe('POST /post', function() {
    it('registreer post', function(done) {
        agent.post('/posts')
            .send({
                'title': 'posttitle',
                'link': 'postlink'
            })
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                expect(res.statusCode).equal(200);
                expect(res.text).be.json;
                var fetchedData = JSON.parse(res.text);
                expect(fetchedData).to.be.an('object');
                expect(fetchedData).to.not.empty;
                expect(fetchedData).to.have.property('token');
                done();
            });
    });
});