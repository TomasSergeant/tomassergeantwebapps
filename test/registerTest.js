var expect = require('chai').expect;
var app = require('../app.js');
var request = require('supertest');
var User = require('../models/Users');
var agent = request.agent(app);


describe('POST /register', function() {
    it('registreer user', function(done) {
        agent.post('/register')
            .send({
                'username': 'testdocent2',
                'password': 'testdocent2'
            })
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                expect(res.statusCode).equal(200);
                expect(res.text).be.json;
                var fetchedData = JSON.parse(res.text);
                expect(fetchedData).to.be.an('object');
                expect(fetchedData).to.not.empty;
                expect(fetchedData).to.have.property('token');
                done();
            });
    });
});